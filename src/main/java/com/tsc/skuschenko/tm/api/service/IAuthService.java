package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    void checkRoles(@Nullable final Role... roles);

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    User registry(
            @Nullable String login, @Nullable String password, @Nullable String email
    );

}
