package com.tsc.skuschenko.tm.api.entity;

import com.tsc.skuschenko.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(Status status);

}
