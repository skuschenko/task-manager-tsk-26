package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessRepository<Task>
        implements ITaskRepository {

    @Nullable
    @Override
    public List<Task> findAllTaskByProjectId(
            @NotNull final String userId, @NotNull final String projectId
    ) {
        return findAll(userId).stream()
                .filter(item -> projectId.equals(item.getProjectId()))
                .collect(Collectors.toList());
    }

}
