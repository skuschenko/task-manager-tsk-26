package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "clear all tasks";

    @NotNull
    private final String NAME = "task-clear";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        taskService.clear(userId);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
