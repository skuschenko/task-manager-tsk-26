package com.tsc.skuschenko.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @SneakyThrows
    @NotNull
    static Integer nextNumber() {
        return Integer.parseInt(nextLine());
    }

}
