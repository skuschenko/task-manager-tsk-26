package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "remove user by login";

    @NotNull
    private final String NAME = "remove-user-by-login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(password);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
