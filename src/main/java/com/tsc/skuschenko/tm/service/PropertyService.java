package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    private static final String AUTHOR_KEY = "author";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getProperties().contains(APPLICATION_VERSION_KEY)) {
            return System.getProperty(APPLICATION_VERSION_KEY);
        }
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY)) {
            return System.getenv(APPLICATION_VERSION_KEY);
        }
        return properties.getProperty(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthor() {
        if (System.getProperties().contains(AUTHOR_KEY)) {
            return System.getProperty(AUTHOR_KEY);
        }
        if (System.getenv().containsKey(AUTHOR_KEY)) {
            return System.getenv(AUTHOR_KEY);
        }
        return properties.getProperty(AUTHOR_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        if (System.getProperties().contains(AUTHOR_EMAIL_KEY)) {
            return System.getProperty(AUTHOR_EMAIL_KEY);
        }
        if (System.getenv().containsKey(AUTHOR_EMAIL_KEY)) {
            return System.getenv(AUTHOR_EMAIL_KEY);
        }
        return properties.getProperty(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getProperties().contains(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value =
                    System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value =
                    System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(
                PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT
        );
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getProperties().contains(PASSWORD_SECRET_KEY)) {
            return System.getProperty(PASSWORD_SECRET_KEY);
        }
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) {
            return System.getenv(PASSWORD_SECRET_KEY);
        }
        return properties.getProperty(
                PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT
        );
    }

}
