package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand abstractCommand);

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull
    AbstractCommand getCommandByArg(@NotNull String name);

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<AbstractCommand> getCommands();

}