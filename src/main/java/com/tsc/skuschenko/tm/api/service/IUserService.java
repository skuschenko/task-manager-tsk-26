package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User findByLogin(@Nullable String login);

    boolean isEmailExist(@Nullable String login);

    boolean isLoginExist(@Nullable String login);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User removeByLogin(String login);

    @NotNull
    User setPassword(String userId, String password);

    @NotNull
    User unlockUserByLogin(String login);

    @NotNull
    User updateUser(
            @Nullable String userId, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

}
